﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    public Bezier bezier;
    public GameObject teleportSprite;

    public bool teleportEnabled;
    public bool firstClick;
    private float firstClickTime;
    private float doubleClickTimeLimit = 0.5f;
    Vector2 touchCoords;
    void Start() {
        teleportEnabled = false;
        firstClick = false;
        firstClickTime = 0f;
        teleportSprite.SetActive(false);
        touchCoords = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    void Update() {
        UpdateTeleportEnabled();

        if (teleportEnabled) {
            HandleBezier();
            HandleTeleport();
        }
    }

    // On double-click, toggle teleport mode on and off.
    void UpdateTeleportEnabled() {
        if (/*OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) ||*/ Input.GetMouseButton(0)) { // The trigger is pressed.
            if (!firstClick) { // The first click is detected.
                firstClick = true;
                firstClickTime = Time.unscaledTime;
            } else { // The second click detected, so toggle teleport mode.
                firstClick = false;
                ToggleTeleportMode();
            }
        }

        if (Time.unscaledTime - firstClickTime > doubleClickTimeLimit) { // Time for the double click has run out.
            firstClick = false;
        }
    }

    void HandleTeleport() {
        if (bezier.endPointDetected) { // There is a point to teleport to.
            // Display the teleport point.
            teleportSprite.SetActive(true);
            teleportSprite.transform.position = bezier.EndPoint;

            if (/*OVRInput.GetDown(OVRInput.Button.One) ||*/Input.GetMouseButton(1)) // Teleport to the position.
                TeleportToPosition(bezier.EndPoint);
        } else {
            teleportSprite.SetActive(false);
        }
    }

    void TeleportToPosition(Vector3 teleportPos) {
        gameObject.transform.position = teleportPos + Vector3.up * 0.5f;
    }

    // Optional: use the touchpad to move the teleport point closer or further.
    void HandleBezier() {
        //Vector2 touchCoords = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))/*OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad)*/;

        if (Mathf.Abs(touchCoords.y) > 0.8f) {
            bezier.ExtensionFactor = touchCoords.y > 0f ? 1f : -1f;
        } else {
            bezier.ExtensionFactor = 0f;
        }
    }

    void ToggleTeleportMode() {
        teleportEnabled = !teleportEnabled;
        bezier.ToggleDraw(teleportEnabled);
        if (!teleportEnabled) {
            teleportSprite.SetActive(false);
        }
    }
}
